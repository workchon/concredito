// ESTA FUNCION SE ENCARGA DE LLAMAR AL SERVICIO CON EL QUE SE OPTIENEN A TODOS LOS PROSPECTOS REGISTRADOS PARA SER MOSTRADOS EN LA TABLA
GetListaProspectos = async() => {
    document.getElementById("FormularioDatos").style.display = "none";
    document.getElementById("FormularioVerDatos").style.display = "none";
    const response = await fetch("http://localhost:8090/api/prospecto", {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
    });
    const data = await response.json();
    const tableBody = document.getElementById("tb");
    //reinicio la tabla en pantalla
    tableBody.innerHTML = "";
    //CREA ROWS
    for (var i = 0; i < data.prospectos.length; i++) {
        const prospecto = data.prospectos[i];
        let row = document.createElement("tr");
        row.innerHTML += "<th>" + prospecto._id + "</th>";
        row.innerHTML += "<td class=\"has-text-centered\">" + prospecto.nombre + "</td>";
        row.innerHTML += "<td class=\"has-text-centered\">" + prospecto.apellidoPaterno + "</td>";
        row.innerHTML += "<td class=\"has-text-centered\">" + prospecto.apellidoMaterno + "</td>";
        row.innerHTML += `<td  class="${(prospecto.estatus == "Enviado" ? "has-text-info" : prospecto.estatus == "Rechazado" ? "has-text-danger" : "has-text-primary")}">  ${prospecto.estatus}  </td>`;
        row.innerHTML += `<td class='align-text'><button ${prospecto.estatus == "Rechazado" ? "disabled" : null} onclick="getProspectoByID_Evaluacion('${prospecto._id}','${prospecto.estatus}')" type="button" class="button is-primary"><i class="fas fa-user-tie"></i></button></td>`;
        row.innerHTML += `<td class='align-text'><button onclick="getProspectoByID('${prospecto._id}','${prospecto.estatus}')" type="button" class="button is-info" ><i class="fas fa-eye"></i>Ver</button></td>`;
        tableBody.appendChild(row);
    }
};


// ESTE VARIABLE SE UTILIZA PARA PODER MOSTRAR LOS PROSPECTOS DE UNA MANERA MAS SENCILLA
const camposVisualizar = [{ "db": "nombre", "input": "vernombre" }, { "db": "apellidoPaterno", "input": "verapellidoPaterno" }, { "db": "apellidoMaterno", "input": "verapellidoMaterno" }, { "db": "calle", "input": "vercalle" }, { "db": "numero", "input": "vernumero" }, { "db": "colonia", "input": "vercolonia" }, { "db": "codigoPostal", "input": "vercodigoPostal" }, { "db": "telefono", "input": "vertelefono" }, { "db": "rfc", "input": "verrfc" }, { "db": "observaciones", "input": "observaciones" }]

//ESTA FUNCION SE UTILIZA PARA OBTENER UN PROSPECTO EN ESPECIFICO PERO SOLO PARA MOSTRAR Y NO EVALUAR
getProspectoByID = async(id, estatus) => {
    seeProspecto(estatus);
    const response = await fetch("http://localhost:8090/api/prospecto/" + id, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
    });

    if (response.status == 200) {
        const data = await response.json();
        let valores = {};
        camposVisualizar.forEach(element => {
            valores[element.input] = document.getElementById(element.input);
        });
        camposVisualizar.forEach(element => {
            valores[element.input].value = data.prospectos[element.db]
        });
        await getListaArchivos(data.prospectos["_id"]);

    }
};

// variable para utilizar de validacion de los datos
const campos = [{ "id": "nombre", "nombre": "Nombre" }, { "id": "apellidoPaterno", "nombre": "Primer apellido" }, { "id": "calle", "nombre": "Calle" }, { "id": "numero", "nombre": "Número:" }, { "id": "colonia", "nombre": "colonia" }, { "id": "codigoPostal", "nombre": "Código postal" }, { "id": "telefono", "nombre": "Teléfono:" }, { "id": "rfc", "nombre": "RFC" }]

// FUNCION QUE SE UTILIZA PARA CAPTURAR UN PROSPECTO, EN ELLA SE REALIZA LA VALIDACION DE LOS CAMPOS
async function GuardarProspecto() {
    const form = document.getElementById("forma");
    let formData = new FormData(form);
    let raw = {};
    for (let i = 0; i < campos.length; i++) {
        if (isNullOrUndefined(formData.get(campos[i].id))) {
            return Swal.fire(
                'Campo obligatorio',
                `El campo ${campos[i].nombre} es obligatorio`,
                'warning'
            )
        } else {
            raw[campos[i].id] = formData.get(campos[i].id);
        }
    }
    raw["apellidoMaterno"] = formData.get("apellidoMaterno")
    raw["estatus"] = "Enviado";

    const fileInput = document.querySelector('#file-js-example input[type=file]');
    if (fileInput.files.length < 1) {
        return Swal.fire(
            'Falta de archivos',
            `Se debe agregar minimo un archivo`,
            'warning'
        )
    }

    const response = await fetch("http://localhost:8090/api/prospecto", {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(raw),
        redirect: 'follow'
    })

    if (response.status == 200) {
        const data = await response.json()
        const archivos = await postSubirArchivo(data.prospecto._id);
        Swal.fire({
            icon: "success",
            title: "Prospecto guardado",
            showConfirmButton: false,
            timer: 1500,
            willClose: function() { location.reload(); },

        });
    }

}

// FUNCION QUE SE UTILIZA PARA EL GUARDADO DE ARCHIVOS
postSubirArchivo = async(id) => {
    const fileInput = document.querySelector('#file-js-example input[type=file]');
    if (fileInput.files.length > 0) {
        for (let i = 0; i < fileInput.files.length; i++) {
            var formdata = new FormData();
            formdata.append("archivo", fileInput.files[i])
            await fetch("http://localhost:8090/api/uploads/" + id, {
                    method: 'POST',
                    redirect: 'follow',
                    body: formdata
                })
                .then(response => console.log(response))
                .then(result => console.log(result))
                .catch(error => console.log('error', error));
        }
    }
}

// FUNCION QUE SE UTILIZA PARA OBTENER EL LISTADO DE ARCHIVOS DEL USUARIO EN ESPECIFICO
getListaArchivos = async(id) => {
        const response = await fetch("http://localhost:8090/api/uploads/" + id, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        });

        if (response.status == 200) {
            const data = await response.json();
            document.getElementById('listFile').innerHTML = "";
            for (var i = 0; i < data.uploads.length; i++) {
                const uploads = data.uploads[i];
                row = document.createElement('div');
                document.getElementById('listFile').append(row);
                row.setAttribute('class', 'column');
                // Create Label
                row.innerHTML = `<button onclick="openFile('${uploads.idProspecto}','${uploads.nombre}')" type="button" class="button is-info" >Archivo:${uploads.nombre}</button>`;
            }

        }
    }
    // FUNCION PARA ABRIR UN ARCHIVO EN ESPECIFICO
openFile = async(id, nombre) => {
    window.open("http://localhost:8090/api/uploads/" + id + "/" + nombre, '_blank');
}



// VARIABLE PARA GUARDAR EL ID DEL USUARIO A EVALUAR
var _idEvaluacion = null;

//FUNCION PARA OBTENER LOS DATOS DEL PROSPECTO A EVALUAR
getProspectoByID_Evaluacion = async(id) => {
    evaluarProspecto()
    const response = await fetch("http://localhost:8090/api/prospecto/" + id, {
        method: "GET",
        headers: {
            "Content-Type": "application/json",
        },
    });

    if (response.status == 200) {
        const data = await response.json();
        let valores = {};
        camposVisualizar.forEach(element => {
            valores[element.input] = document.getElementById(element.input);
        });
        camposVisualizar.forEach(element => {
            valores[element.input].value = data.prospectos[element.db]
        });

        _idEvaluacion = data.prospectos["_id"]
        await getListaArchivos(_idEvaluacion);
        console.log(_idEvaluacion);
    }
};

//FUNCION QUE SE UTILIZA PARA ACTUALIZAR EL ESTATUS DEL PROSPECTO SEGUN SU EVALUACION
putUpdateProspecto = async(estatus) => {

    if (estatus == 'Rechazado') {
        if (isNullOrUndefined(document.getElementById("observaciones").value)) {
            return Swal.fire(
                'Campo obligatorio',
                `El campo observaciones es obligatorio`,
                'warning'
            )
        }
    }

    var raw = JSON.stringify({
        "observaciones": document.getElementById("observaciones").value.length > 0 ? document.getElementById("observaciones").value : '',
        "estatus": estatus
    });
    evaluarProspecto()
    const response = await fetch("http://localhost:8090/api/prospecto/" + _idEvaluacion, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json",
        },
        body: raw
    });

    if (response.status == 200) {
        Swal.fire({
            icon: "success",
            title: 'Evaluación',
            text: `El Prospecto fue ${estatus}`,
            showConfirmButton: false,
            timer: 1500,
            willClose: function() { location.reload(); },

        })
    }
};


window.onload = GetListaProspectos()