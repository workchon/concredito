//ESTA ES UNA FUNCION PARA QUE EL BURGER DE LA NAVBAR FUNCIONE
document.addEventListener('DOMContentLoaded', () => {

    // Get all "navbar-burger" elements
    const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

    // Check if there are any navbar burgers
    if ($navbarBurgers.length > 0) {

        // Add a click event on each of them
        $navbarBurgers.forEach(el => {
            el.addEventListener('click', () => {

                // Get the target from the "data-target" attribute
                const target = el.dataset.target;
                const $target = document.getElementById(target);

                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                el.classList.toggle('is-active');
                $target.classList.toggle('is-active');

            });
        });
    }

});


// ESTA FUNCION SE UTILIZA PARA REVISAR SI EL DATO ES ES NULL, UNDEFINED O TIENE UN STRING DE 0
function isNullOrUndefined(dato) {
    if (dato === undefined || dato === null || dato.toString().trim().length <= 0) {
        return true;
    }
    return false
}

// ESTA FUNCION SE ENCARGA DE HACER MOSTRAR LA TABLA DE LOS PROSPECTOS A EVALUAR
function OpenTable() {
    document.getElementById("tablaDatos").style.display = "block";
    document.getElementById("FormularioDatos").style.display = "none";
    document.getElementById("FormularioVerDatos").style.display = "none";
    document.getElementById("capturar").removeAttribute("disabled");
    GetListaProspectos();
}

// ESTA FUNCION SE UTILIZA PARA MOSTRAR EL FORMULARIO PARA CAPTURAR AL PROSPECTO
function addProspecto() {
    document.getElementById("FormularioDatos").style.display = "block";
    document.getElementById("tablaDatos").style.display = "none";
    document.getElementById("FormularioVerDatos").style.display = "none";
    document.getElementById("forma").reset()
    document.getElementById("capturar").setAttribute("disabled", "disabled");

    let fileInput = document.querySelector('#file-js-example input[type=file]');
    fileInput.files = null;
    let fileName = document.querySelector('#file-js-example .nombresArchivos');
    fileName.innerHTML = "";
}

// ESTA FUNCION SE UTILIZA PARA MOSTRAR AL PROSPECTO EN SUS TRES ESTATUS
function seeProspecto(estatus) {
    document.getElementById("FormularioVerDatos").style.display = "block";
    document.getElementById("tablaDatos").style.display = "none";
    document.getElementById("FormularioDatos").style.display = "none";
    document.getElementById("evaluar").style.display = "none";
    if (estatus != "Rechazado") {
        document.getElementById("observacion").style.display = "none";
        document.getElementById("observaciones").removeAttribute("disabled");
    } else {
        document.getElementById("observacion").style.display = "block";
        document.getElementById("observaciones").setAttribute("disabled", "disabled");
    }

}

// ESTA FUNCION SE UTILIZA PARA MOSTRAR A LOS PROSPECTOS QUE SERAN EVALUADOS
function evaluarProspecto() {
    document.getElementById("FormularioVerDatos").style.display = "block";
    document.getElementById("evaluar").style.display = "block";
    document.getElementById("observacion").style.display = "block";
    document.getElementById("observaciones").removeAttribute("disabled");
    document.getElementById("observaciones").value = "";

    document.getElementById("tablaDatos").style.display = "none";
    document.getElementById("FormularioDatos").style.display = "none";
    document.getElementById("volver").style.display = "none";

}

// ESTA FUNCION LIMPIA EL FORMULARIO CUANDO SE CANCELA LA CAPTURA DEL PROSPECTO
function limpiarFormulario(redirect = false) {
    Swal.fire({
        title: 'Al salir los datos no se guardaran',
        showDenyButton: true,
        confirmButtonText: `Cancelar captura de prospecto`,
        denyButtonText: `No cancelar captura de prospecto`,
    }).then((result) => {
        if (result.isConfirmed) {
            document.getElementById("forma").reset()
            if (redirect) {
                OpenTable()
            }
        }
    })

}

// ESTA FUNCION SE UTILIZA PARA VER SI EL VALOR INCERTADO EN EL INPUT ES NUMERICO
function isNumericKey(evt, element, max_chars = 5) {

    if (element.value.length <= max_chars) {
        var charCode = evt.which ? evt.which : evt.keyCode;
        if ((charCode = 46 && charCode > 31 && (charCode < 48 || charCode > 57)))
            return false;
        return true;
    }

}