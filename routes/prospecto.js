const { Router } = require('express');
const { postGuardarProspectos, getProspectos, getProspectoById, putActualizarProspecto } = require('../controllers/prospecto.controller');

const router = Router();

// SERVICIO QUE SE UTILIZA PARA GUARDAR AL PROSPECTO
router.post('/', [], postGuardarProspectos);

//SERVICIO QUE SE UTILIZA PARA TRAER A TODOS LOS PROSPECTOS
router.get('/', getProspectos);

//SERVICIO QUE SE UTILIZA PARA TRAER A UN PROSPECTO EN ESPECIFICO
router.get('/:id', getProspectoById);

//SERVICIO QUE SE UTILIZA PARA ACTUALIZAR A UN PROSPECTO EN ESPECIFICO
router.put('/:id', putActualizarProspecto);




module.exports = router;