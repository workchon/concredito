const { Router } = require('express');
const { cargarArchivo, mostrarImagen, getArchivoByIdProspecto } = require('../controllers/upload.controller');


const router = Router();

// SERVICIO QUE SE UTILIZA PARA GUARDAR EL ARCHIVO
router.post('/:id', cargarArchivo)
    //SERVICIO QUE SE UTILIZA PARA MOSTRAR UN ARCHIVO EN ESPECIFICO
router.get('/:id/:nombre', mostrarImagen)
    // SERVICIO PARA OBTENER EL LISTADO DE ARCHIVOS POR PROSPECTO
router.get('/:id', getArchivoByIdProspecto)
module.exports = router;