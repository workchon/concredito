const { Schema, model } = require('mongoose');
// ESQUEMA DEL LA TABLA PARA ARCHIVOS
const UploadsSchema = Schema({
    nombre: {
        type: String,
        require: [true, 'El nombre es obligatorio']
    },
    idProspecto: {
        type: String,
        require: [true, 'El id del prospecto es obligatorio']
    }

});


module.exports = model('Uploads', UploadsSchema)