const { Schema, model } = require('mongoose');
// ES EL ESQUEMA DEL LA TABLA DEL PROSPECTO
const ProspectoSchema = Schema({
    nombre: {
        type: String,
        require: [true, 'El nombre es obligatorio']
    },
    apellidoPaterno: {
        type: String,
        require: [true, 'El primer apellido es obligatorio']
    },
    apellidoMaterno: {
        type: String
    },
    calle: {
        type: String,
        require: [true, 'la calle es obligatorio']
    },
    numero: {
        type: String,
        require: [true, 'El número es obligatorio']
    },
    colonia: {
        type: String,
        require: [true, 'la colonia es obligatorio']
    },
    codigoPostal: {
        type: String,
        require: [true, 'El codigo postal es obligatorio']
    },
    telefono: {
        type: String,
        require: [true, 'El es obligatorio es obligatorio']
    },
    rfc: {
        type: String,
        require: [true, 'El rfc es obligatorio'],
    },
    estatus: {
        type: String
    },
    observaciones: {
        type: String
    }

});


module.exports = model('Prospecto', ProspectoSchema)