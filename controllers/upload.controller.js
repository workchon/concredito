const { response } = require("express");
const { subirArchivo } = require("../helpers/subir-archivo");
const path = require('path')
const fs = require('fs');
const Uploads = require("../models/uploads");

// SE UTILIZA PARA GUARDA LOS ARCHVISO DEL PROSPECTO
const cargarArchivo = async(req, res = response) => {
    const { id } = req.params;

    if (!req.files || Object.keys(req.files).length === 0 || !req.files.archivo) {
        res.status(400).send('No files were uploaded.');
        return;
    }
    try {
        const pathArchivo = await subirArchivo(req.files, id);
        const uploads = new Uploads({ nombre: pathArchivo, idProspecto: id });
        await uploads.save();

        res.json({
            nombre: pathArchivo
        })
    } catch (msg) {
        res.status(400).json({ msg })
    }
}

// SE UTILIZA PARA MOSTRAR UN ARCHIVO EN ESPECIFICO
const mostrarImagen = async(req, res = response) => {

    const { id, nombre } = req.params;
    const pathImagen = path.join(__dirname, '../uploads', id, nombre);
    console.log(pathImagen)
    if (fs.existsSync(pathImagen)) {
        return res.sendFile(pathImagen)
    }
}

// SE UTILIZA PARA OBTENER EL LISTADO DE LOS ACHIVOS POR PROSPECTO
const getArchivoByIdProspecto = async(req, res = response) => {

    const { id } = req.params;
    const uploads = await Uploads.find({ idProspecto: id })
    res.json({
        uploads
    });
}

module.exports = {
    cargarArchivo,
    mostrarImagen,
    getArchivoByIdProspecto
}