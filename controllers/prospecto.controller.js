const { response } = require('express');

const Prospecto = require('../models/prospecto')

// UTILIZADO PARA GUARDAR AL PROSPECTO
const postGuardarProspectos = async(req, res = response) => {
    const { nombre, apellidoPaterno, apellidoMaterno = "", calle, numero, colonia, codigoPostal, telefono, rfc, estatus, observaciones = "" } = req.body;
    console.log(req.body)
        //const body = req.body;
    const prospecto = new Prospecto({ nombre, apellidoPaterno, apellidoMaterno, calle, numero, colonia, codigoPostal, telefono, rfc, estatus, observaciones });

    await prospecto.save();

    res.json({
        msg: 'get API',
        prospecto
    });
}

//  TRAER EL LISTADO DE PROSPECTOS
const getProspectos = async(req, res = response) => {

    const { limite = 999, desde = 0, } = req.query;
    const prospectos = await Prospecto.find()
        .skip(Number(desde))
        .limit(Number(limite));
    res.json({
        prospectos
    });
}

// TRAER LOS DATOS EXPECIFICOS DE UN PROSPECTO
const getProspectoById = async(req, res = response) => {

    const { id } = req.params;
    const prospectos = await Prospecto.findById(id)
    res.json({
        prospectos
    });
}

// ACTUALIZA LOS DATOS DE UN PROSPECTO
const putActualizarProspecto = async(req, res = response) => {
    const { id } = req.params;
    const { estatus, observaciones } = req.body;

    const prospecto = await Prospecto.findByIdAndUpdate(id, { estatus, observaciones })

    res.json({
        msg: 'get API',
        prospecto
    });
}


module.exports = {
    getProspectos,
    postGuardarProspectos,
    getProspectoById,
    putActualizarProspecto
}