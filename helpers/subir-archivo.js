const path = require('path')

// FUNCION UTILIZADA PARA GUARDAR LOS ARCHIVOS, SE SEPARO DEL SERVICIO PARA PODER SER UTILIZADA POR OTRAS FUNCIONES SIN 
// TENER QUE VOLVER A REPETIR EL CODIGO
const subirArchivo = (files, id) => {

    return new Promise((resolve, reject) => {
        const { archivo } = files;
        const nombreArchivo = archivo.name;
        const uploadPath = path.join(__dirname, '../uploads/', id, nombreArchivo);

        archivo.mv(uploadPath, (err) => {
            if (err) {
                return reject(err)
            }

            resolve(nombreArchivo)
        });

    })

}


module.exports = {
    subirArchivo
}